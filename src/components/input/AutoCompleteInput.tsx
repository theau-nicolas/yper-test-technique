import { useRef } from "react";

import { LoadScript, StandaloneSearchBox } from "@react-google-maps/api";

const AutoCompleteInput = ({
  onAutoComplete,
}: {
  onAutoComplete: Function;
}) => {
  const inputRef = useRef<any>();

  const handlePlaceChanged = () => {
    const [place] = inputRef.current.getPlaces();
    if (place) {
      // console.log(place.formatted_address);
      // console.log(place.geometry.location.lat());
      // console.log(place.geometry.location.lng());
    }
    onAutoComplete({
      lat: place.geometry.location.lat(),
      long: place.geometry.location.lng(),
    });
  };

  return (
    <LoadScript
      googleMapsApiKey={process.env.REACT_APP_GOOGLE_API_KEY || ""}
      libraries={["places"]}
    >
      <StandaloneSearchBox
        onLoad={(ref) => (inputRef.current = ref)}
        onPlacesChanged={handlePlaceChanged}
      >
        <input
          type="text"
          name="address"
          className="form-control p-3 bg-light rounded border-0"
          placeholder="Votre adresse"
        />
      </StandaloneSearchBox>
    </LoadScript>
  );
};

export default AutoCompleteInput;
