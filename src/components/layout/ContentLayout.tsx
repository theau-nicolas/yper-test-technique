import React, { PropsWithChildren } from "react";
import NavBar from "./NavBar";

type Props = PropsWithChildren<{}>;

const ContentLayout = ({ children }: Props) => {
  return (
    <>
      <NavBar />
      <div className="container w-75">{children}</div>
    </>
  );
};

export default ContentLayout;
