import { useQuery } from "react-query"
import { getRetailPoint, getRetailPoints } from "../../services/retailpoints.service"

export const retailpointKeys = {
    retailPoints : "retailPoints",
    retailPoint: "retailPoint"
}

export const keyFactory = {
    retailPoints : () => [retailpointKeys.retailPoints],
    retailPoint : (id : string) => [retailpointKeys.retailPoint, id]
}

export const useRetailPointsQuery = ({location,enabled}: {location:string ,enabled:boolean}) => {
    return useQuery(keyFactory.retailPoints(), () => getRetailPoints({
        location,
        max_distance: 30000,
        limit: 10
    }), {
        enabled
    })
}

export const useRetailPointDetailQuery = ({id, enabled}: {id:string, enabled:boolean}) => {
    return useQuery(keyFactory.retailPoint(id), () => getRetailPoint({id}),{
        enabled
    })
}
