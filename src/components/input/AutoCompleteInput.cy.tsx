import React from "react";
import AutoCompleteInput from "./AutoCompleteInput";

describe("<AutoCompleteInput />", () => {
  it("renders", () => {
    // see: https://on.cypress.io/mounting-react
    cy.mount(
      <AutoCompleteInput
        onAutoComplete={(lat: string, long: string) =>
          console.log(`location : ${lat}, ${long}`)
        }
      />
    );
  });
});
