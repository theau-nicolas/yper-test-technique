import axios from "axios"

const config = {
    headers: {
        'Authorization' : 'Bearer ' + process.env.REACT_APP_API_TOKEN,
        'X-Request-Timestamp' : Date.now().toString()
      }
}

export const getRetailPoints = async ({
    location,
    min_distance,
    max_distance,
    limit
  }: {
    location: string;
    min_distance?: number;
    max_distance?: number;
    limit?: number
  }): Promise<any> => {

    const endPoint = "/retailpoint/search";
  
    const params: { location: string; min_distance?: string; max_distance?: string; limit?: string } = { location };

    min_distance && (params.min_distance = min_distance.toString());
    max_distance && (params.max_distance = max_distance.toString());
    limit && (params.limit = limit.toString());

    const payload = new URLSearchParams(params)
  
    return await axios.get(
      `${process.env.REACT_APP_API_BASE_URL}${endPoint}?${payload}`,
      config
    );
  };

export const getRetailPoint = async ({id}: {id:string}): Promise<any> => {

    const endPoint = "/retailpoint";

    return await axios.get(
        `${process.env.REACT_APP_API_BASE_URL}${endPoint}/${id}`,
        config
    );
};