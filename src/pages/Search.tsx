import { MouseEvent, useEffect, useState } from "react";
import AutoCompleteInput from "../components/input/AutoCompleteInput";
import { useRetailPointsQuery } from "../hooks/retailPoints/retailPoints.queries";
import { useSearchParams } from "react-router-dom";

export default function Search() {
  const [hasChangedLocation, setHasChangeLocation] = useState(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const [enableSearch, setEnableSearch] = useState(
    !!searchParams.get("location") ? true : false
  );

  const { data, refetch } = useRetailPointsQuery({
    location: searchParams.get("location") || "",
    enabled: enableSearch,
  });

  const onSearch = (e: MouseEvent) => {
    if (!!searchParams.get("location") && hasChangedLocation) {
      setEnableSearch(true);
      refetch();
    }
    setHasChangeLocation(false);
  };

  const onAutoComplete = ({ lat, long }: { lat: number; long: number }) => {
    setSearchParams({ location: `${lat},${long}` });
    setHasChangeLocation(true);
  };

  useEffect(() => {}, [searchParams]);

  return (
    <div>
      <div className="container-sm mb-5">
        <section className="row align-items-center">
          <div className="col ">
            <h1 className="fs-1 mb-5 fw-bold">
              Trouvez les points de vente proches de chez vous !
            </h1>
            <p className="text-muted fs-5 fw-light">
              Renseignez votre adresse dans le champ ci-dessous, et trouvez tous
              nos points de vente en quelques instants :{" "}
            </p>
            <div className="input-group p-0 m-0 row">
              <div className="col-8 p-0">
                <AutoCompleteInput onAutoComplete={onAutoComplete} />
              </div>
              <button
                className="btn btn-outline-secondary col fw-bold"
                type="button"
                id="button-addon2"
                onClick={onSearch}
              >
                Rechercher
              </button>
            </div>
          </div>
          <img
            className="col-5"
            style={{ width: "44%" }}
            src="/images/pat.png"
            alt="enseigne"
          />
        </section>
      </div>

      <div className="container muted p-5 ">
        {!data?.data.result ? (
          <div className="text-center">
            <img src="/images/pin.png" alt="pin" className="mb-4 .mx-auto" />
            <h2 className="fw-bold fs-5 text-muted">
              Lancez une recherche pour afficher les points de vente ici !
            </h2>
          </div>
        ) : (
          <>
            <h2 className="mb-4">Résultats de la recherche :</h2>
            <ul className="list-group">
              {data?.data.result.map((pv: any) => {
                return (
                  <>
                    <div className="divider" />
                    <li
                      key={pv._id}
                      className="p-4 fw-bold list-group-item border-0 muted d-flex flex-row justify-content-between"
                    >
                      <p>{pv.name}</p>
                      <a href={`/retailpoint/${pv._id}`} className="fw-light">
                        Voir plus d'infos
                      </a>
                    </li>
                  </>
                );
              })}
            </ul>
          </>
        )}
      </div>
    </div>
  );
}
