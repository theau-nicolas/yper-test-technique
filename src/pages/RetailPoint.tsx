import { useParams } from "react-router-dom";
import { useRetailPointDetailQuery } from "../hooks/retailPoints/retailPoints.queries";
import { format, getHours } from "date-fns";
import Map from "../components/map/Map";
import { useNavigate } from "react-router-dom";
import Dash from "../components/icons/Dash";

export const WEEK_DAY = [
  "Lundi",
  "Mardi",
  "Mercredi",
  "Jeudi",
  "Vendredi",
  "Samedi",
  "Dimanche",
];

export default function RetailPoint() {
  const navigate = useNavigate();

  let { id } = useParams();

  const { data } = useRetailPointDetailQuery({
    id: id?.toString() || "",
    enabled: !!id,
  });

  const goBack = () => navigate(-1);

  const retailDetails = data?.data?.result;

  if (!retailDetails) {
    return null;
  }

  return (
    <div className="container-md mb-5">
      <section className="row">
        <section className="col">
          <h1>{retailDetails.name}</h1>
          <p>{retailDetails.address.street}</p>
          <p>
            {retailDetails.address.zip} {retailDetails.address.city}
          </p>
        </section>
        <section className="col">
          <Map
            lat={retailDetails.address.location.coordinates[1]}
            long={retailDetails.address.location.coordinates[0]}
          />
        </section>
      </section>
      <section>
        <h2>Horaires d'ouvertures</h2>
        <ul className="p-5 muted mb-5 list-group">
          {retailDetails.delivery_hours.map((day: any, i: number) => {
            return (
              <li className="p-4 fw-bold  border-0 muted row  ">
                <span className="col fw-light">{WEEK_DAY[day.day - 1]}</span>
                <span className="col fw-light">
                  {getHours(new Date(day.hours.start))}h - 12h
                </span>
                <span className="col fw-light">
                  14h - {getHours(new Date(day.hours.end))}h{" "}
                </span>
                <span className="col fw-light">
                  {day.isOpened === true
                    ? "ouvert"
                    : day.isOpened === false
                    ? "fermé"
                    : null}
                </span>
              </li>
            );
          })}
        </ul>
      </section>
      <div className="divider mb-4" />
      <a href="#" onClick={goBack}>
        <Dash />
        <span className="m-3">Retour aux résultats</span>
      </a>
    </div>
  );
}
