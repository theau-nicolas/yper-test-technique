import React from "react";
import ContentLayout from "./components/layout/ContentLayout";
import Search from "./pages/Search";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RetailPoint from "./pages/RetailPoint";
import "./style/main.css";

function App() {
  return (
    <ContentLayout>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Search />} />
          <Route path="/retailpoint">
            <Route path=":id" element={<RetailPoint />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </ContentLayout>
  );
}

export default App;
